require('dotenv').load();

var jsonfile = require('jsonfile');
var fs = require('fs');

const Discord = require('discord.js');

const bot = new Discord.Client();
const token = process.env.API_KEY;

var CHANNELS_WHITELIST = ['tests', 'zone51'];
var RULES;

function wordInString(s, word) {
	//return new RegExp('\\b' + word + '\\b', 'i').test(s);
	return s.indexOf(word) > -1;
}

function wordsInString(s, words)  {

	for (var id in words) {
		var word = words[id];

		if (wordInString(s, word)) return true;
	}

	return false;
}

function loadPhrases() {
	jsonfile.readFile('data/rules.json', function(err, obj) {
		RULES = obj;
		if (!err) console.log("Loaded rules from data/rules.json");
	})
}

function savePhrases() {
	jsonfile.writeFile('data/rules.json', RULES, function(err) {
		if (!err) console.log("Saved rules to data/rules.json");
		else console.error("Error while saving rules :/");
	})
}

bot.on('ready', () => {
	console.log('Discord.js bot started');
	console.log('Username: ' + bot.user.username);

	loadPhrases();
});

bot.on('message', message => {

	if (message.author.username === bot.user.username) return;

	//Si on est pas sur un channel de DM	
	if (!(message.channel instanceof Discord.DMChannel)) {
		var channelName = message.channel.name;

		//Si c'est un serveur sur lequel le bot peut parler
		if (CHANNELS_WHITELIST.indexOf(channelName) != -1) {
			var content = message.content;

			if (content == '/feminazi keywords') {

				var str = '[' + Object.keys(RULES).length + ' keywords]\n';

				//On parcours les règles et si ça match on balance une phrase
				for (var ruleKeyword in RULES) {
					var nb = RULES[ruleKeyword].length;
					str += ruleKeyword + "(" + nb + ")\n";
				}
				message.channel.sendMessage(str);
			} else if (content == '/feminazi rules') {

				var str = '';

				//On parcours les règles et si ça match on balance une phrase
				for (var ruleKeyword in RULES) {
					var rule = RULES[ruleKeyword];
					str += '*' + ruleKeyword + '\n';

					rule.map(function(value) {
						str += '\t\t*' + value + '\n';
					});
				}
				message.channel.sendMessage(str);
			} else if (content.startsWith('/clap')) {
				var tokens = content.split(' ');

				if (tokens.length < 2) return;

				message.channel.sendMessage(tokens.splice(1, tokens.length).join(' :clap: '));
			} else if (content.startsWith('/h ')) {
				var tokens = content.split(' ');

				phrase = tokens.splice(1, tokens.length).join(' ').toLowerCase();

				var str = '';
				for (var i = 0; i < phrase.length; i++) {
					var c = phrase[i];

					if (c >= 'a' && c <= 'z')
						str += ':regional_indicator_' + c + ':';
					else if (c === ' ')
						str += '  ';
					else
						str += c;
				}

				message.channel.sendMessage(str);

			} else if (wordsInString(content.toLowerCase(), ['anus', 'cul', 'fesse', 'rondelle', 'rectum', 'trou', 'anal', 'backdoor', 'goatse'])) {

				fs.readFile('data/goatse.txt', 'utf8', function(err, contents) {
					message.channel.sendMessage('```' + contents + '```');
				});
			} else if (content.startsWith('/feminazi add')) {
				console.log('Command from: ' + message.author.username.toLowerCase())
				if (['rgehan', 'vertag', 'hakim'].indexOf(message.author.username.toLowerCase()) <= -1) {
					console.log("Can't add rule. User not authorized...");
					message.channel.sendMessage("T'as pas les droits uesh, tu te crois où");
					return;
				}

				var tokens = content.split(' ');

				if (tokens.length > 3) {
					var keyword = tokens[2];
					var phrase = tokens.slice(3, tokens.length).join(' ');

					console.log(keyword + '  =>  ' + phrase);

					if (keyword in RULES) {
						RULES[keyword].push(phrase);
					} else {
						RULES[keyword] = [phrase];
					}

					savePhrases();
				}
			} else {

				//On parcours les règles et si ça match on balance une phrase
				for (var ruleKeyword in RULES) {
					if (wordInString(content.toLowerCase(), ruleKeyword)) {
						var rule = RULES[ruleKeyword];

						var count = rule.length;
						var pick = Math.floor(Math.random() * count);

						//message.reply(rule[pick]);
						message.channel.sendMessage(rule[pick]);
					}
				}

			}
		}

	} else {

		console.log('[Message] from @' + message.author.username + '; Msg: ' + message.content);
		message.reply("C'est quoi cette micro-agression. Sors de mon safe-space cis-white-shitlord!");
	}
});

// log our bot in
bot.login(token);